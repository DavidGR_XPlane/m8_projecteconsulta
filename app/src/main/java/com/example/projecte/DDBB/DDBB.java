package com.example.projecte.DDBB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DDBB extends SQLiteOpenHelper {
    String createDoctorTable = "CREATE TABLE " + DDBB_Info.taula_metge + " (MetgeId integer primary key autoincrement, " + DDBB_Info.columna_metge_nom + " TEXT, " + DDBB_Info.columna_metge_cognom + " TEXT, " + DDBB_Info.columna_metge_email + " TEXT, " + DDBB_Info.columna_metge_disponibilitat + " TEXT)";
    String createClientsTable = "CREATE TABLE " + DDBB_Info.taula_clients + " (" +
            DDBB_Info.columna_client_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DDBB_Info.columna_client_dni + " TEXT, " +
            DDBB_Info.columna_client_email + " TEXT, " +
            DDBB_Info.columna_client_password + " TEXT, " +
            DDBB_Info.columna_client_nom + " TEXT, " +
            DDBB_Info.columna_client_cognom1 + " TEXT, " +
            DDBB_Info.columna_client_cognom2 + " TEXT, " +
            DDBB_Info.columna_client_idmetge + " INTEGER, " +
            "FOREIGN KEY(" + DDBB_Info.columna_client_idmetge + ") REFERENCES " + DDBB_Info.taula_metge + "(" + DDBB_Info.columna_metge_id + ")" +
            ")";

    String createAppointmentTable = "CREATE TABLE " + DDBB_Info.taula_cites + " (" +
            DDBB_Info.columna_cita_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DDBB_Info.columna_client_id + " INTEGER, " +
            DDBB_Info.columna_client_idmetge + " INTEGER, " +
            DDBB_Info.columna_cita_dataIhora + " TEXT, " +
            DDBB_Info.columna_cita_descripcio + " TEXT, " +
            "FOREIGN KEY(" + DDBB_Info.columna_client_id + ") REFERENCES " + DDBB_Info.taula_clients + "(" + DDBB_Info.columna_client_id + "), " +
            "FOREIGN KEY(" + DDBB_Info.columna_client_idmetge + ") REFERENCES " + DDBB_Info.taula_metge + "(" + DDBB_Info.columna_metge_id + ")" +
            ")";


    // DROP
    String dropDoctorTable = "DROP TABLE IF EXISTS " + DDBB_Info.taula_metge;
    String dropClientsTable = "DROP TABLE IF EXISTS " + DDBB_Info.taula_clients;
    String dropAppointmentsTable = "DROP TABLE IF EXISTS " + DDBB_Info.taula_cites;

    // DADES D'EXEMPLE
    String inserirDoctors = "INSERT INTO Doctors (nom, cognom, email, disponibilitat) VALUES " +
            "('Dr. Joan', 'García', 'juan@gmail.com','Disponible')," +
            "('Dra. María', 'López', 'maria@gmail.com', 'No disponible')," +
            "('Dr. Carles', 'Martínez', 'carlos@gmail.com', 'Disponible')," +
            "('Dra. Laura', 'Fernández', 'laura@gmail.com', 'Disponible')," +
            "('Dr. Martí', 'Comas', 'mcomasc@gmail.com','No disponible')";
    String inserirClients = "INSERT INTO " + DDBB_Info.taula_clients + " (" +
            DDBB_Info.columna_client_dni + ", " +
            DDBB_Info.columna_client_email + ", " +
            DDBB_Info.columna_client_password + ", " +
            DDBB_Info.columna_client_nom + ", " +
            DDBB_Info.columna_client_cognom1 + ", " +
            DDBB_Info.columna_client_cognom2 + ", " +
            DDBB_Info.columna_client_idmetge +
            ") VALUES " +
            "('11111111A', 'usuari1@example.com', 'david', 'Joan', 'Pérez', 'González', 1), " +
            "('22222222B', 'usuari2@example.com', 'david', 'María', 'López', 'Martínez', 2), " +
            "('33333333C', 'usuari3@example.com', 'david', 'Carles', 'García', 'Fernández', 3), " +
            "('44444444D', 'usuari4@example.com', 'david', 'Ana', 'Martínez', 'Sánchez', 1), " +
            "('54756529S', 'davidgr.damvi@gmail.com', 'david', 'David', 'Gómez', 'Raya', 5), " +
            "('55555555E', 'usuari5@example.com', 'david', 'David', 'Ruiz', 'Gómez', 2)";

    String inserirCites = "INSERT INTO " + DDBB_Info.taula_cites + " (" +
            DDBB_Info.columna_client_id + ", " +
            DDBB_Info.columna_client_idmetge + ", " +
            DDBB_Info.columna_cita_dataIhora + ", " +
            DDBB_Info.columna_cita_descripcio + ") VALUES " +
            "(1, 1, '2024-2-15 10:00:00', 'Consulta de seguiment per controlar la pressió arterial.'), " +
            "(2, 2, '2024-2-16 11:00:00', 'Examen físic anual i anàlisi de sang per detectar possibles malalties.'), " +
            "(3, 3, '2024-2-17 12:00:00', 'Consulta per revisar el tractament de la diabetis i ajustar la dosi d''insulina.'), " +
            "(5, 5, '2024-2-18 12:30:00', 'Consulta per revisar el tractament de la diabetis i ajustar la dosi d''insulina.'), " +
            "(5, 5, '2024-2-19 11:00:00', 'Examen físic anual i anàlisi de sang per detectar possibles malalties.'), " +
            "(5, 3, '2024-2-20 11:00:00', 'Seguiment postoperatori per verificar la cicatrització i la recuperació.'), " +
            "(4, 1, '2024-2-21 13:00:00', 'Seguiment postoperatori per verificar la cicatrització i la recuperació.'), " +
            "(5, 2, '2024-2-22 14:00:00', 'Consulta prenatal per controlar el desenvolupament del fetus i la salut de la mare.');";


    public DDBB(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createDoctorTable);
        sqLiteDatabase.execSQL(createClientsTable);
        sqLiteDatabase.execSQL(createAppointmentTable);
        sqLiteDatabase.execSQL(inserirDoctors);
        sqLiteDatabase.execSQL(inserirClients);
        sqLiteDatabase.execSQL(inserirCites);

        /* afegir dades */
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(dropDoctorTable);
        sqLiteDatabase.execSQL(dropClientsTable);
        sqLiteDatabase.execSQL(dropAppointmentsTable);
        sqLiteDatabase.execSQL(createDoctorTable);
        sqLiteDatabase.execSQL(createClientsTable);
        sqLiteDatabase.execSQL(createAppointmentTable);
        sqLiteDatabase.execSQL(inserirDoctors);
        sqLiteDatabase.execSQL(inserirClients);
        sqLiteDatabase.execSQL(inserirCites);
    }
}
