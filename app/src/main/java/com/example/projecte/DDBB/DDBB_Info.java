package com.example.projecte.DDBB;

public class DDBB_Info {
    public static final String nomBBDD = "CatSalutBona";
    public static final int versionBBDD = 15;

    /* CLIENTS */
    public static final String taula_clients = "Clients";
    public static final String columna_client_id = "ClientId";
    public static final String columna_client_idmetge = "id_doctor";
    public static final String columna_client_dni = "dni";
    public static final String columna_client_email = "email";
    public static final String columna_client_password = "password";
    public static final String columna_client_nom = "nom";
    public static final String columna_client_cognom1 = "cognom1";
    public static final String columna_client_cognom2 = "cognom2";

    /* DOCTORS */
    public static final String taula_metge = "Doctors";
    public static final String columna_metge_id = "MetgeId";
    public static final String columna_metge_nom = "nom";
    public static final String columna_metge_cognom = "cognom";
    public static final String columna_metge_email = "email";
    public static final String columna_metge_disponibilitat = "disponibilitat";

    /* CITES */
    public static final String taula_cites = "Cites";
    public static final String columna_cita_id = "CitaId";
    public static final String columna_cita_descripcio = "Descripcio";
    public static final String columna_cita_dataIhora = "Data";


    }