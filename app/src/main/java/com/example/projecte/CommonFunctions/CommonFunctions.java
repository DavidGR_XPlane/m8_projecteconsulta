package com.example.projecte.CommonFunctions;

import android.content.Context;
import android.widget.Toast;

public class CommonFunctions {

    public static void makeToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void finish() {
        finish();
    }

}
