package com.example.projecte.Activities.Menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.projecte.Activities.Menu.ConsultarCites;
import com.example.projecte.Activities.Menu.MetgeFamilia;
import com.example.projecte.Activities.Menu.SolicitarCites;
import com.example.projecte.Activities.Settings.Settings_;
import com.example.projecte.DDBB.DDBB;
import com.example.projecte.DDBB.DDBB_Info;
import com.example.projecte.R;

public class MainMenu extends AppCompatActivity {

    DDBB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        initButtons();
        db = new DDBB(this, DDBB_Info.nomBBDD, null, DDBB_Info.versionBBDD);

        TextView benvingudaNom = findViewById(R.id.mm_txtV_Benvingut);

        putName(benvingudaNom);
    }

    private void putName(TextView textView) {
        SQLiteDatabase readableDatabase = db.getReadableDatabase();
        SharedPreferences sharedPreferences = getSharedPreferences("SessioUsuari", MODE_PRIVATE);

        String nom = "";

        String[] args = {String.valueOf(sharedPreferences.getInt("SessioUsuariID", 54))};
        Cursor cursor = readableDatabase.rawQuery("SELECT nom FROM " + DDBB_Info.taula_clients + " Clients WHERE ClientId = ?", args);
        assert cursor != null;
        if (cursor.moveToNext()) {
            nom = cursor.getString(0).toLowerCase();
        }
        String primer = nom.substring(0, 1).toUpperCase();
        String segon = nom.substring(1);
        String nomFinal = primer + segon;
        Log.i("nom", nom);
        String textFinal = textView.getText() + " " + nomFinal;
        textView.setText(textFinal);
    }

    private void logOut() {
        SharedPreferences sharedPreferences = getSharedPreferences("SessioUsuari", MODE_PRIVATE);
        SharedPreferences.Editor sharedPrefs = sharedPreferences.edit();
        sharedPrefs.putInt("SessioUsuariID", -1);
        sharedPrefs.apply();
        finish();
    }

    private void StartMetgeFamilia() {
        Intent i = new Intent(this, MetgeFamilia.class);
        startActivity(i);
    }

    private void StartCites() {
        Intent i = new Intent(this, ConsultarCites.class);
        startActivity(i);
    }

    private void StartSolicitarCites() {
        Intent i = new Intent(this, SolicitarCites.class);
        startActivity(i);
    }

    private void StartSettings() {
        Intent i = new Intent(this, Settings_.class);
        startActivity(i);
    }

    private void initButtons() {
        ImageButton btn_logout = findViewById(R.id.mm_btn_logout);
        btn_logout.setOnClickListener(view -> logOut());
        ImageButton btn_metgeFamilia = findViewById(R.id.mm_btn_metgeFamilia);
        btn_metgeFamilia.setOnClickListener(view -> StartMetgeFamilia());
        ImageButton btn_cites = findViewById(R.id.mm_btn_cites);
        btn_cites.setOnClickListener(view -> StartCites());
        ImageButton btn_solicitarCita = findViewById(R.id.mm_btn_solicicitaCita);
        btn_solicitarCita.setOnClickListener(view -> StartSolicitarCites());
        ImageButton btn_settings = findViewById(R.id.mm_btn_settings);
        btn_settings.setOnClickListener(view -> StartSettings());
    }

}