package com.example.projecte.Activities.Menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.projecte.DDBB.DDBB;
import com.example.projecte.DDBB.DDBB_Info;
import com.example.projecte.R;

public class MetgeFamilia extends AppCompatActivity {

    DDBB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metge_familia);
        db = new DDBB(this, DDBB_Info.nomBBDD, null, DDBB_Info.versionBBDD);
        initButtons();
        cercarDadesMetgeFamilia();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initButtons();
        cercarDadesMetgeFamilia();
    }

    private void cercarDadesMetgeFamilia() {
        String id_Doctor = cercarIdDoctor();

        String nom = "";
        String cognom = "";
        String disponibilitat = "";

        TextView nomMetge = findViewById(R.id.mf_txtV_NomMetge);
        TextView cognomMetge = findViewById(R.id.mf_txtV_CognomMetge);
        TextView disponibilitatMetge = findViewById(R.id.mf_txtV_DisponibilitatMetge);

        SQLiteDatabase readableDatabase = db.getReadableDatabase();
        String[] args1 = {id_Doctor};
        Cursor cursor = readableDatabase.rawQuery("SELECT nom, cognom, disponibilitat FROM " + DDBB_Info.taula_metge + " WHERE MetgeId = ?", args1);
        if (cursor.moveToNext()) {
            nom = cursor.getString(0);
            cognom = cursor.getString(1);
            disponibilitat = cursor.getString(2);
        }
        nomMetge.setText(nom);
        cognomMetge.setText(cognom);
        disponibilitatMetge.setText(disponibilitat);
    }

    private String cercarIdDoctor() {
        SQLiteDatabase readableDatabase = db.getReadableDatabase();
        SharedPreferences sharedPreferences = getSharedPreferences("SessioUsuari", MODE_PRIVATE);
        String[] args1 = {String.valueOf(sharedPreferences.getInt("SessioUsuariID", 54))};
        String id_doctor = "";
        Cursor cursor = readableDatabase.rawQuery("SELECT id_doctor FROM " + DDBB_Info.taula_clients + " WHERE ClientId = ?", args1);
        if (cursor.moveToNext())
            id_doctor = String.valueOf(cursor.getInt(0));
        return id_doctor;
    }

    private void enviarCorreu() {
        SQLiteDatabase readableDatabase = db.getReadableDatabase();
        String[] args1 = {cercarIdDoctor()};
        String correu = "";
        Cursor cursor = readableDatabase.rawQuery("SELECT email FROM " + DDBB_Info.taula_metge + " WHERE MetgeId = ?", args1);
        if (cursor.moveToNext())
            correu = cursor.getString(0);
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        Uri uri = Uri.parse("mailto:" + correu);
        intent.setData(uri);
        startActivity(intent);
    }

    private void initButtons() {
        ImageButton btn_logout = findViewById(R.id.mf_btn_exit);
        btn_logout.setOnClickListener(view -> finish());
        ImageButton btn_correu = findViewById(R.id.mf_imgbtn_correu);
        btn_correu.setOnClickListener(view -> enviarCorreu());
    }

}