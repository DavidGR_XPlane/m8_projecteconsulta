package com.example.projecte.Activities.Menu;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.projecte.DDBB.DDBB;
import com.example.projecte.DDBB.DDBB_Info;
import com.example.projecte.R;

public class SolicitarCites extends AppCompatActivity {

    DDBB db;
    TextView escullDate;
    CalendarView calendar;
    TextView escullMotiu;
    Spinner spinner;
    String motiu;
    ImageButton btn_next;
    ImageButton btn_previous;
    TextView escullHora;
    TimePicker timePicker;
    ImageButton btn_cita;
    String diaFinal;
    String horaFinal;


    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitar_cites);
        db = new DDBB(this, DDBB_Info.nomBBDD, null, DDBB_Info.versionBBDD);
        escullDate = findViewById(R.id.solCi_txtV_escull);
        calendar = findViewById(R.id.solCi_calendar);
        calendar.setOnDateChangeListener((calendarView, year, month, dayOfMonth) -> {
            diaFinal = year + "-" + (month + 1) + "-" + dayOfMonth;
        });
        escullMotiu = findViewById(R.id.solCi_txtV_motiu);
        spinner = findViewById(R.id.solCi_spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                motiu = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btn_next = findViewById(R.id.solCi_btn_next);
        btn_previous = findViewById(R.id.solCi_btn_previous);
        escullHora = findViewById(R.id.solCi_txtV_escullHora);
        timePicker = findViewById(R.id.solCi_timePicker);
        timePicker.setOnTimeChangedListener((timePicker, hourOfDay, minute) -> horaFinal = String.format("%02d:%02d:00", hourOfDay, minute));
        btn_cita = findViewById(R.id.solCi_btn_finishCita);
        visibilitatHora(View.GONE);
        initButtons();
    }

    private void initButtons() {
        ImageButton btn_exit = findViewById(R.id.solci_btn_exit);
        btn_exit.setOnClickListener(view -> finish());
        btn_previous.setOnClickListener(view -> previous());
        btn_cita.setOnClickListener(view -> demanarCita());
        btn_next.setOnClickListener(view -> next());

    }

    private void demanarCita() {
        SharedPreferences sharedPreferences = getSharedPreferences("SessioUsuari", MODE_PRIVATE);
        int usuariID = sharedPreferences.getInt("SessioUsuariID", 54);
        SQLiteDatabase writableDatabase = db.getWritableDatabase();


        String id_doctor = "";
        String[] args1 = {String.valueOf(usuariID)};
        Cursor cursor = writableDatabase.rawQuery("SELECT id_doctor FROM " + DDBB_Info.taula_clients + " WHERE ClientId = ?", args1);
        if (cursor.moveToNext()) id_doctor = String.valueOf(cursor.getInt(0));
        cursor.close();

        ContentValues novaCita = new ContentValues();
        novaCita.put(DDBB_Info.columna_client_id, usuariID);
        novaCita.put(DDBB_Info.columna_client_idmetge, id_doctor);
        novaCita.put(DDBB_Info.columna_cita_dataIhora, diaFinal + " " + horaFinal);
        novaCita.put(DDBB_Info.columna_cita_descripcio, motiu);
        writableDatabase.insert(DDBB_Info.taula_cites, null, novaCita);
        finish();
    }

    private void previous() {
        visibilitatData(View.VISIBLE);
        visibilitatHora(View.GONE);
    }

    private void next() {
        visibilitatData(View.GONE);
        visibilitatHora(View.VISIBLE);
    }


    private void visibilitatData(int visibility) {
        escullDate.setVisibility(visibility);
        calendar.setVisibility(visibility);
        btn_next.setVisibility(visibility);
        escullMotiu.setVisibility(visibility);
        spinner.setVisibility(visibility);
    }

    private void visibilitatHora(int visibility) {
        escullHora.setVisibility(visibility);
        timePicker.setVisibility(visibility);
        btn_cita.setVisibility(visibility);
        btn_previous.setVisibility(visibility);
    }

}