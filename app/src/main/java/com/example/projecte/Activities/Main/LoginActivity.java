package com.example.projecte.Activities.Main;

import static java.lang.String.valueOf;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projecte.Activities.Menu.MainMenu;
import com.example.projecte.CommonFunctions.CommonFunctions;
import com.example.projecte.DDBB.DDBB;
import com.example.projecte.DDBB.DDBB_Info;
import com.example.projecte.R;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    DDBB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initButtons();

        db = new DDBB(this, DDBB_Info.nomBBDD, null, DDBB_Info.versionBBDD);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        TextView email = findViewById(R.id.LO_eTxt_email);
        TextView password = findViewById(R.id.LO_eTxtPassword);
        TextView dni = findViewById(R.id.LO_eTxt_DNI);
        email.setText("");
        password.setText("");
        dni.setText("");
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.LO_btn_acceptarLog) {
            SQLiteDatabase writableDatabase = db.getWritableDatabase();
            String email = ((TextView) findViewById(R.id.LO_eTxt_email)).getText().toString().toLowerCase();
            String password = ((TextView) findViewById(R.id.LO_eTxtPassword)).getText().toString();
            String[] args = new String[]{email.toLowerCase(), password};
            Log.w("Base de dades oberta? ", valueOf(writableDatabase.isOpen()));
            Cursor cursor = writableDatabase.rawQuery("SELECT ClientId, nom FROM Clients WHERE email = ? AND password = ?", args);

            if (cursor.moveToNext()) {
                CommonFunctions.makeToast("Credencials correctes", this);
                SharedPreferences sharedPreferences = getSharedPreferences("SessioUsuari", MODE_PRIVATE);
                SharedPreferences.Editor sharedPrefs = sharedPreferences.edit();
                sharedPrefs.putInt("SessioUsuariID", cursor.getInt(0));
                sharedPrefs.putString("SessioUsuariNOM", cursor.getString(1));
                sharedPrefs.apply();
                Log.w("tag", String.valueOf(cursor.getString(1)));
                Log.w("tag", String.valueOf(sharedPreferences.getInt("SessioUsuariID", 54)));
                Intent i = new Intent(this, MainMenu.class);
                startActivity(i);
                cursor.close();
            } else {
                CommonFunctions.makeToast("Credencials incorrectes", this);
            }
        }
        if (id == R.id.LO_btn_cancelarLog) {
            finish();
        }
    }

    private void initButtons() {
        ArrayList<Button> buttons = new ArrayList<>();
        buttons.add(findViewById(R.id.LO_btn_acceptarLog));
        buttons.add(findViewById(R.id.LO_btn_cancelarLog));
        buttons.forEach(btn -> btn.setOnClickListener(this));
    }

}