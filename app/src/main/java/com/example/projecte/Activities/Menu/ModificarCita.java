package com.example.projecte.Activities.Menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.projecte.DDBB.DDBB;
import com.example.projecte.DDBB.DDBB_Info;
import com.example.projecte.R;

public class ModificarCita extends AppCompatActivity {

    DDBB db;
    TextView data;
    TextView metge;
    TextView descripcio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_cita);
        db = new DDBB(this, DDBB_Info.nomBBDD, null, DDBB_Info.versionBBDD);
        initViews();
        initButtons();
        fillFields();
    }


    private void cancelarCita() {
        String dataBuscar = data.getText().toString();
        String metgeBuscar = metge.getText().toString();
        String descripcioBuscar = descripcio.getText().toString();
        buscarEliminarCita(dataBuscar, metgeBuscar, descripcioBuscar);
    }

    private void buscarEliminarCita(String data, String metge, String descripcio) {
        SQLiteDatabase writableDatabase = db.getWritableDatabase();
        String metgeId = buscarMetgeId(writableDatabase, metge);

        SharedPreferences sharedPreferences = getSharedPreferences("SessioUsuari", MODE_PRIVATE);
        int clientId = sharedPreferences.getInt("SessioUsuariID", 54);
        String[] whereArgs = {String.valueOf(clientId), metgeId, data, descripcio};
        int rowsAffected = writableDatabase.delete("Cites", "ClientId = ? AND id_doctor = ? AND Data = ? AND Descripcio = ?", whereArgs);
        if (rowsAffected > 0) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "channel_id")
                    .setSmallIcon(R.drawable.calendario_pedir)
                    .setContentTitle(getString(R.string.NotifCites))
                    .setContentText(getString(R.string.NotifCitaCancelada))
                    .setAutoCancel(true);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel("channel_id", "Channel Name", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            notificationManager.notify(1, builder.build());
            finish();
        }
    }

    private String buscarMetgeId(SQLiteDatabase writableDatabase, String metge) {
        //primer busquem al metge de la cita
        String metgeId = "";
        String[] nomMetge = metge.split(" ");
        String[] args = new String[]{nomMetge[0] + " " + nomMetge[1], nomMetge[2]};
        Cursor cur_trobarMetge = writableDatabase.rawQuery("SELECT MetgeId FROM Doctors WHERE nom = ? AND cognom = ?", args);
        if (cur_trobarMetge.moveToNext()) {
            metgeId = String.valueOf(cur_trobarMetge.getInt(0));
        }
        return metgeId;
    }

    private void initViews() {
        data = findViewById(R.id.modCi_txtV_DataSel);
        metge = findViewById(R.id.modCi_txtV_DocSel);
        descripcio = findViewById(R.id.modCi_txtV_DescSel);

    }

    private void initButtons() {
        ImageButton btn_exit = findViewById(R.id.modCI_btn_exit);
        btn_exit.setOnClickListener(view -> finish());
        ImageButton btn_cancelar = findViewById(R.id.modCi_btn_eliminarCita);
        btn_cancelar.setOnClickListener(view -> cancelarCita());
    }

    private void fillFields() {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        data.setText(bundle.getString("Data"));
        metge.setText(bundle.getString("Facultatiu"));
        descripcio.setText(bundle.getString("Descripcio"));
    }

}