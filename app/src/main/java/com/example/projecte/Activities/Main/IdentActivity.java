package com.example.projecte.Activities.Main;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.projecte.DDBB.DDBB;
import com.example.projecte.DDBB.DDBB_Info;
import com.example.projecte.R;

import java.util.ArrayList;

public class IdentActivity extends AppCompatActivity implements View.OnClickListener {

    DDBB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ident_activity);
        initButtons();
        db = new DDBB(this, DDBB_Info.nomBBDD, null, DDBB_Info.versionBBDD);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ID_btn_register) {
            Intent i = new Intent(this, RegisterActivity.class);
            startActivityIfNeeded(i, 1);
        }
        if (id == R.id.ID_btn_login) {
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 1) {
//            CommonFunctions.makeToast("Usuari afegit correctament!", this);
//        }
    }

    private void initButtons() {
        ArrayList<Button> buttons = new ArrayList<>();
        buttons.add(findViewById(R.id.ID_btn_register));
        buttons.add(findViewById(R.id.ID_btn_login));
        buttons.forEach(btn -> btn.setOnClickListener(this));
    }

}