package com.example.projecte.Activities.Utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.widget.AdapterView;

import java.util.Locale;

public class Utils {

    public static void changeLanguage(Context context, String idioma) {
        Locale locale = new Locale(idioma);
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.setLocale(locale);
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

}
