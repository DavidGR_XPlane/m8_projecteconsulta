package com.example.projecte.Activities.Menu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projecte.DDBB.DDBB;
import com.example.projecte.DDBB.DDBB_Info;
import com.example.projecte.Entities.Cita;
import com.example.projecte.R;

import java.util.ArrayList;

public class ConsultarCites extends AppCompatActivity {

    DDBB db;
    ArrayList<Cita> cites = new ArrayList<>();
    ListView llistaCites;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cites);
        db = new DDBB(this, DDBB_Info.nomBBDD, null, DDBB_Info.versionBBDD);
        initButtons();
        refrescarLlista();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        refrescarLlista();
    }

    private void refrescarLlista() {
        SQLiteDatabase readableDatabase = db.getReadableDatabase();
        AdaptadorCita adaptador = new AdaptadorCita(this);
        cercarDadesCita(readableDatabase);
        llistaCites = findViewById(R.id.ci_lstV_cites);
        llistaCites.setAdapter(adaptador);
        llistaCites.setOnItemClickListener((arg0, arg1, arg2, arg3) -> {
            Intent intent = new Intent(getApplicationContext(), ModificarCita.class);
            Bundle bundle = new Bundle();
            Cita cita = (Cita) arg0.getItemAtPosition(arg2);
            bundle.putString("Data", cita.getData());
            bundle.putString("Facultatiu", cita.getFacultatiu());
            bundle.putString("Descripcio", cita.getDescripcio());
            intent.putExtras(bundle);
            startActivity(intent);
        });
    }

    private void cercarDadesCita(SQLiteDatabase readableDatabase) {
        cites.clear();
        SharedPreferences sharedPreferences = getSharedPreferences("SessioUsuari", MODE_PRIVATE);
        String[] cols = new String[]{"Data", "Descripcio", "id_doctor"};
        String selection = "ClientId = ?";
        String[] args = new String[]{String.valueOf(sharedPreferences.getInt("SessioUsuariID", 54))};
        Cursor cursor = readableDatabase.query(DDBB_Info.taula_cites, cols, selection, args, null, null, null);
        int i = 0;
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Log.i("i", String.valueOf(i));
                Cita cita = new Cita(cursor.getString(0), cursor.getString(1));
                String nom = cercarMetge(readableDatabase, cursor.getString(2));
                cita.setFacultatiu(nom);
                cites.add(cita);
                i++;
            } while (cursor.moveToNext());
        }
        assert cursor != null;
        cursor.close();
    }

    private String cercarMetge(SQLiteDatabase readableDatabase, String id) {
        String nomCognom = "";
        String[] args = new String[]{id};
        Cursor cursor = readableDatabase.rawQuery("SELECT nom, cognom FROM " + DDBB_Info.taula_metge + " WHERE MetgeId = ?", args);
        if (cursor != null && cursor.moveToNext()) {
            nomCognom = cursor.getString(0) + " " + cursor.getString(1);
        }
        assert cursor != null;
        cursor.close();
        return nomCognom;
    }

    private void initButtons() {
        ImageButton btn_logout = findViewById(R.id.ci_btn_exit);
        btn_logout.setOnClickListener(view -> finish());
    }

    public class AdaptadorCita extends ArrayAdapter<Cita> {
        Activity context;

        public AdaptadorCita(Activity context) {
            super(context, R.layout.cita_format, cites);
            this.context = context;
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            Log.i("i", "faig getView()");
            LayoutInflater inflater = context.getLayoutInflater();
            @SuppressLint("ViewHolder") View item = inflater.inflate(R.layout.cita_format, null);
            TextView txt_Data = item.findViewById(R.id.txtData);
            txt_Data.setText(cites.get(position).getData());
            TextView txt_Metge = item.findViewById(R.id.txtMetge);
            txt_Metge.setText(cites.get(position).getFacultatiu());
            TextView txt_Desc = item.findViewById(R.id.txtDescripcio);
            txt_Desc.setText(cites.get(position).getDescripcio());
            return (item);
        }
    }

}