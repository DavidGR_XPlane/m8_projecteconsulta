package com.example.projecte.Activities.Main;

import static java.lang.String.valueOf;

import com.example.projecte.CommonFunctions.CommonFunctions;
import com.example.projecte.DDBB.DDBB;
import com.example.projecte.DDBB.DDBB_Info;
import com.example.projecte.R;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import kotlin.random.Random;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    DDBB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        db = new DDBB(this, DDBB_Info.nomBBDD, null, DDBB_Info.versionBBDD);
        initButtons();
    }

    private void initButtons() {
        ArrayList<Button> buttons = new ArrayList<>();
        buttons.add(findViewById(R.id.RE_btn_Cancelar));
        buttons.add(findViewById(R.id.RE_btn_Register));
        buttons.forEach(button -> button.setOnClickListener(this));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.RE_btn_Register) {
            SQLiteDatabase writableDatabase = db.getWritableDatabase();
            SQLiteDatabase readableDatabase = db.getReadableDatabase();

            String email = ((TextView) findViewById(R.id.RE_eTxt_email)).getText().toString().toLowerCase();
            String password = ((TextView) findViewById(R.id.RE_eTxt_password)).getText().toString();
            String nom = ((TextView) findViewById(R.id.RE_eTxt_nom)).getText().toString().toLowerCase();
            String primerCognom = ((TextView) findViewById(R.id.RE_eTxt_cognom)).getText().toString().toLowerCase();
            String segonCognom = ((TextView) findViewById(R.id.RE_eTxt_cognom2)).getText().toString().toLowerCase();
            String dni = ((TextView) findViewById(R.id.RE_eTxt_DNI)).getText().toString().toLowerCase();

            boolean acceptat = ((CheckBox) findViewById(R.id.RE_chkB_Condicions)).isChecked();

            if (checkFields(email, password, nom, primerCognom, segonCognom, dni, acceptat)) {
                if (!duplicatedUser(readableDatabase, dni)) {
                    addUser(writableDatabase, email, password, nom, primerCognom, segonCognom, dni);
                } else {
                    CommonFunctions.makeToast("Aquest usuari ja existeix!", this);
                }
            } else {
                if (!acceptat)
                    CommonFunctions.makeToast("Has d'acceptar les condicions per a continuar", this);
                else
                    CommonFunctions.makeToast("Revisa els camps, possiblement hi hagi un error", this);
            }
        }
        if (id == R.id.RE_btn_Cancelar) {
            finish();
        }
    }

    /**
     * Comprova que els camps no estiguin buits
     */
    private boolean checkFields(String email, String password, String nom, String cognom, String segonCognom, String dni, boolean acceptat) {
        boolean totOk = true;
        if (!(email.trim().contains("@")) && email.trim().equalsIgnoreCase(""))
            totOk = false;
        if (password.trim().equalsIgnoreCase(""))
            totOk = false;
        if (nom.trim().equalsIgnoreCase(""))
            totOk = false;
        if (cognom.trim().equalsIgnoreCase(""))
            totOk = false;
        if (segonCognom.trim().equalsIgnoreCase(""))
            totOk = false;
        if (dni.trim().equalsIgnoreCase(""))
            totOk = false;
        if (!acceptat)
            totOk = false;
        return totOk;
    }

    /**
     * Comproba si hi ha algún usuari repetit, mirant el seu DNI.
     */
    private boolean duplicatedUser(SQLiteDatabase writableDatabase, String DNI) {
        String[] args = new String[]{DNI.toUpperCase()};
        Log.w("Base de dades oberta? ", valueOf(writableDatabase.isOpen()));
        Cursor cursor = writableDatabase.rawQuery("SELECT email FROM Clients WHERE DNI = ?", args);
        boolean duplicated = cursor.moveToFirst();
        cursor.close();
        return duplicated;
    }

    /**
     * Afegeix un usuari
     */
    private void addUser(SQLiteDatabase writableDatabase, String email, String password, String nom, String primerCognom, String segonCognom, String dni) {

        ContentValues nouRegistre = new ContentValues();

        nouRegistre.put("dni", dni.toUpperCase());
        nouRegistre.put("email", email.toLowerCase());
        nouRegistre.put("password", password);
        nouRegistre.put("nom", nom.toUpperCase());
        nouRegistre.put("cognom1", primerCognom.toUpperCase());
        nouRegistre.put("cognom2", segonCognom.toUpperCase());
        nouRegistre.put("id_doctor", ThreadLocalRandom.current().nextInt(0, 5));
        writableDatabase.insert(DDBB_Info.taula_clients, null, nouRegistre);
        finish();
    }


}