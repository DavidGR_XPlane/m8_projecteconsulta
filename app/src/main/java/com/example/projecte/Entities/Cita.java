package com.example.projecte.Entities;

import androidx.annotation.NonNull;

public class Cita {

    public String data;
    public String descripcio;
    public String facultatiu;

    public Cita() {

    }

    public Cita(String data, String descripcio) {
        this.data = data;
        this.descripcio = descripcio;
    }

    public String getFacultatiu() {
        return facultatiu;
    }

    public void setFacultatiu(String facultatiu) {
        this.facultatiu = facultatiu;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @NonNull
    @Override
    public String toString() {
        return "Cita{" +
                "data='" + data + '\'' +
                ", descripcio='" + descripcio + '\'' +
                '}';
    }
}
